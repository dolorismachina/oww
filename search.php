<?php
require('header.php');
require('wine.php');

function getAll()
{
    global $db;
    $results = array();

    $res = $db->query("SELECT * FROM wine");
    if ($res->num_rows > 0)
    {
        for ($i = 0; $i < $res->num_rows; $i++)
        {
            $results[$i] = $res->fetch_assoc();
        }
    }

    return $results;
}

function getAllOfColour($colour)
{
    global $db;
    $results = array();

    $query = "SELECT * FROM wine WHERE colour = '$colour';";

    $res = $db->query($query);
    if ($res->num_rows > 0)
    {
        for ($i = 0; $i < $res->num_rows; $i++)
        {
            $results[$i] = $res->fetch_assoc();
        }
    }

    return $results;
}

function getAllOfTypeAndCountry($type, $country)
{
    global $db;
    $results = array();

    $res = $db->query("SELECT * FROM wine WHERE type = '$type' AND country = '$country';");
    if ($res->num_rows > 0)
    {
        for ($i = 0; $i < $res->num_rows; $i++)
        {
            $results[$i] = $res->fetch_assoc();
        }
    }

    return $results;
}

function getAllOfType($type)
{
    global $db;
    $results = array();

    $res = $db->query("SELECT * FROM wine WHERE type = '$type';");
    {
        for ($i = 0; $i < $res->num_rows; $i++)
        {
            $results[$i] = $res->fetch_assoc();
        }
    }

    return $results;
}

function getAllOfCountry($country)
{
    global $db;
    $results = array();

    $res = $db->query("SELECT * FROM wine WHERE country = '$country';");
    for ($i = 0; $i < $res->num_rows; $i++)
    {
        $results[$i] = $res->fetch_assoc();
    }

    return $results;
}

if (!isset($_GET['type']) && !isset($_GET['country']) && !isset($_GET['colour'])) // No search terms at all.
{
    $results = getAll();
}
else if (isset($_GET['country']) && isset($_GET['type'])) // Product searched by type and country.
{
    $country = strtolower($_GET['country']);
    $type = strtolower($_GET['type']);
    foreach (getAllOfTypeAndCountry($type, $country) as $r)
    {
        displayWine($r);
    }
}
else if (isset($_GET['country']))
{
    $country = strtolower($_GET['country']);
    foreach (getAllOfCountry($country) as $r)
    {
        displayWine($r);
    }
}
else if (isset($_GET['type']))
{
    $type = strtolower($_GET['type']);

    foreach (getAllOfType($type) as $r)
    {
        displayWine($r);
    }
}
else if (isset($_GET['colour']))
{
    $colour = strtolower($_GET['colour']);

    foreach (getAllOfColour($colour) as $r)
    {
        displayWine($r);
    }
}
?>

<?php

function displayWine($wine)
{
    global $db;
    $query = "SELECT * FROM stock WHERE wine_id = '" . $wine['id'] . "';";
    $result = $db->query($query);
    if ($result->num_rows == 1)
    {
        $row = $result->fetch_assoc();
        $stock = $row['quantity'];
    }
    ?>
    <article class = "wine">
        <section class = "wine_description">
            <h1><?php echo $wine['name']; ?></h1>
            <p><?php echo $wine['description']; ?></p>
        </section>
        <section class = "wine_details">
            <p>Price: <?php echo $wine['bottle_price']; ?></p>
            <p>Country of origin: <?php echo $wine['country']; ?></p>
            <p>Availability: <?php echo $row['quantity']; ?> </p>
    <?php
    if ($stock > 0)
    {
        ?>
                <form action = "basket.php" method = "get">
                    <input type = "number" value = "1" class = "wine_add_quantity" max = "99" name = "quantity" />
                    <input type = "hidden" value = "add" name = "action" />
                    <input type = "hidden" name = "id" value = "<?php echo $wine['id']; ?>" />
                    <input type = "submit" />
                </form>
        <?php
    }
    ?>
        </section>
    </article>
        <?php }
        ?>

<?php require('footer.php'); ?>