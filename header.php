<?php
require_once 'db_setup.php';
require_once 'customer.php';
require_once 'html.php';


function showLoginForm()
{ ?>
    <form action = "login.php" method = "post" id = "login_form">
        <label for = "username">Username</label>
        <input type = "text" name = "username" />
        
        <label for = "password">Password</label>
        <input type = "password" name = "password" />
        
        <input type = "submit" value = "Login" />
    </form>

    <a href = "register.php">Register</a>
<?php }

function showUserPanel()
{ ?>
    <p>Welcome back <?php echo $_SESSION['user']->username; ?></p>
    <a href = "basket.php">Basket <?php echo "(" . $_SESSION['items'] . ")"; ?></a>
    <a href = "logout.php">Logout</a>
<?php }
?>

<!DOCTYPE html>

<html lang = "en">
<head>
    <link rel = "stylesheet" type = "text/css" href = "style.css">
    <meta charset = "utf-8" />
    <title>Welcome to Online Wine Warehouse</title>
</head>

<body>
    <div id = "main">
    <div id = "logo">
        <a href = "index.php">Wine Warehouse</a>
    </div>
    <div id = "header">
    <?php
    if (isset($_SESSION['user']))
    {
        $query = "SELECT * FROM shopping_basket WHERE customer_id = '" . $_SESSION['user']->id . "';";
        $result = $db->query($query);
        $_SESSION['items'] = $result->num_rows;
        showUserPanel();
    }
    else
    {
        showLoginForm();
    }
    ?>
    </div>

    <?php require('sidemenu.php'); ?>
    <div id = "content">