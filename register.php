<?php
require_once 'header.php';
require_once 'validation.php';
?>

<?php
$validation = new Validation();

if (fieldsSet())
{
    if (inputValid())
    {
        if (userExists($_POST['username']))
        {
            echo "Chosen username is already taken. Try another one.";
        }
        if (emailExists($_POST['email']))
        {
            echo "Chosen email is already taken. Try another one.";
        }
        else
        {
            // Add the user to the database.
            $query = "INSERT INTO customer VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, NULL)";
            if (!$stmt = $db->prepare($query))
            {
                echo "Couldn't prepare the statement.";
            }
            else if (!$stmt->bind_param("ssssssss", $_POST['first_name'],
                    $_POST['last_name'],
                    $_POST['username'],
                    $_POST['password'],
                    $_POST['street'],
                    $_POST['city'],
                    $_POST['postcode'],
                    $_POST['email']))
            {
                echo $stmt->error;
            }
            else if (!$stmt->execute())
            {
                echo $stmt->error;
            }
            else
            {
                echo "You have been successfully registered. You can now log in.";
                echo '<a href = "index.php"> Go back to home page</a>';
            }
            $stmt->close();
        }
    }
    else
    {
        global $validation;
        echo $validation->getErrorMessage();
        echo $errorMessage;
    }
}

function inputValid()
{
    global $validation;
    
    $validation->validateUsername($_POST['username']);
    $validation->validateEmail($_POST['email'], $_POST['email_confirm']);
    
    $validation->validatePassword($_POST['password'], $_POST['password_confirm']);
    $validation->validateName($_POST['first_name'], $_POST['last_name']);
    $validation->validateAddress($_POST['street'], $_POST['city'], $_POST['postcode']);
    
    return $validation->getErrorMessage() === "";
}

function userExists($username)
{
    global $db;
    $query = "SELECT * FROM customer WHERE username = '" . $username . "';";
    $result = $db->query($query);
    
    return $result->num_rows > 0;
}

function emailExists($email)
{
    global $db;
    $query = "SELECT * FROM customer WHERE email_address = '" . $email . "';";
    $result = $db->query($query);
    
    return $result->num_rows > 0;
}

// Ensures the validation doesn't take place before the form is submitted.
function fieldsSet()
{
    return isset($_POST['username']) && 
           isset($_POST['password']) &&
           isset($_POST['email']) &&
           isset($_POST['first_name']) &&
           isset($_POST['last_name']) && 
           isset($_POST['street']) &&
           isset($_POST['postcode']) &&
           isset($_POST['city']);
}

if (isset($_SESSION['user']))
{
    echo "<a href = 'index.php'>You shouldn't be here. Click to go back to homepage.</a>";
}
else
{
?>

<p>All fields are required</p>
<form action = "register.php" method = "post" id ="register_form">
    <label for = "username">Username</label>
    <input type = "text" name = "username" id = "username" maxlength="16" />

    <label for = "email">Email</label>
    <input type = "email" name = "email" id = "email" />

    <label for = "email_confirm">Email confirmation</label>
    <input type = "text" name = "email_confirm" id = "email_confirm" />

    <label for = "password">Password</label>
    <input type = "password" name = "password" id = "password" maxlength="32" />

    <label for = "password_confirm">Password confirmation</label>
    <input type = "password" name = "password_confirm" id = "password_confirm" maxlength="32" />

    <label for = "first_name">First name</label>
    <input type = "text" name = "first_name" id = "first_name" />

    <label for = "last_name">Last name</label>
    <input type = "text" name = "last_name" id = "last_name" />

    <label for = "street">Street </label>
    <input type = "text" name = "street" id = "street" />

    <label for = "postcode">Postcode</label>
    <input type = "text" name = "postcode" id = "postcode" maxlength="9" />

    <label for = "city">City</label>
    <input type = "text" name = "city" id = "city" maxlength="50" />

    <input type = "submit" value = "Register" />
</form>

<?php
} // else
require_once('footer.php');