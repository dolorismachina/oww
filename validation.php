<?php

class Validation
{

    private $errorMessage = "";

    public function validateUsername($username)
    {
        if ($this->isEmpty($username))
        {
            $this->errorMessage .= '<p class = "errorMessage">Username field is empty.</p>';
        }
        else if ($this->isTooLong($username, 16))
        {
            $this->errorMessage .= '<p class = "errorMessage">Username is longer than 16 characters.</p>';
        }
        else if ($this->isTooShort($username, 3))
        {
            $this->errorMessage .= '<p class = "errorMessage">Username is shorter than 3 characters.</p>';
        }
        else if (preg_match("/[^a-zA-Z0-9_]/", $username))
        {
            $this->errorMessage .= '<p class = "errorMessage">Username contains invalid characters. Only alphanumeric characters and underscore "_" are allowed.</p>';
        }

        return true;
    }

    public function validateEmail($email, $emailConfirmation)
    {
        if ($this->isEmpty($email))
        {
            $this->errorMessage .= '<p class = "errorMessage">Email is empty.</p>';
        }
        else if (!preg_match("/[^\s@]+@[^\s@]+\.[^\s@]+/", $email))
        {
            $this->errorMessage .= '<p class = "errorMessage">Email pattern is incorrect.</p>';
        }
        else if ($email !== $emailConfirmation)
        {
            $this->errorMessage .= '<p class = "errorMessage">Email confirmation doesn\'t match.</p>';
        }
    }

    public function validatePassword($password, $passwordConfirm)
    {
        if ($this->isEmpty($password))
        {
            $this->errorMessage .= '<p class = "errorMessage">Password field is empty.</p>';
        }
        else if ($this->isTooLong($password, 32))
        {
            $this->errorMessage .= '<p class = "errorMessage">Password is longer than 32 characters.</p>';
        }
        else if ($this->isTooShort($password, 6))
        {
            $this->errorMessage .= '<p class = "errorMessage">Password is shorter than 6 characters.</p>';
        }
        else if (preg_match("/[^a-zA-Z0-9_]/", $password))
        {
            $this->errorMessage .= '<p class = "errorMessage">Password contains invalid characters. Use alphanumeric characters and underscore\'_\'.</p>';
        }
        else if ($password !== $passwordConfirm)
        {
            $this->errorMessage .= '<p class = "errorMessage">Passwords don\'t match.</p>';
        }
    }

    public function validateName($firstName, $lastName)
    {
        $this->validateFirstName($firstName);
        $this->validateLastName($lastName);
    }

    private function validateFirstName($firstName)
    {
        if ($this->isEmpty($firstName))
        {
            $this->errorMessage .= '<p class = "errorMessage">First name is empty.</p>';
        }
        else if ($this->isTooShort($firstName, 2))
        {
            $this->errorMessage .= '<p class = "errorMessage">First name is shorter than 2 characters.</p>';
        }
        else if (preg_match("/[^A-Za-z]/", $firstName))
        {
            $this->errorMessage .= '<p class = "errorMessage">First name contains invalid characters. Use alphanumeric characters and underscore\'_\'.</p>';
        }
    }

    private function validateLastName($lastName)
    {
        if ($this->isEmpty($lastName))
        {
            $this->errorMessage .= '<p class = "errorMessage">Last name is empty.</p>';
        }
        else if ($this->isTooShort($lastName, 2))
        {
            $this->errorMessage .= '<p class = "errorMessage">Last name is shorter than 2 characters.</p>';
        }
        else if (preg_match("/[^A-Za-z]/", $lastName))
        {
            $this->errorMessage .= '<p class = "errorMessage">Last name contains invalid characters. Use alphanumeric characters and underscore\'_\'.</p>';
        }
    }

    public function validateAddress($street, $city, $postcode)
    {
        $this->validateStreet($street);
        $this->validatePostcode($postcode);
        $this->validateCity($city);
    }

    private function validatePostcode($postcode)
    {
        if ($this->isEmpty($postcode))
        {
            $this->errorMessage .= '<p class = "errorMessage">You didn\'t enter postcode.</p>';
        }
        else if ($this->isTooLong($postcode, 9))
        {
            $this->errorMessage .= '<p class = "errorMessage">Postcode is too long.</p>';
        }
        else if (preg_match('/[^a-zA-Z\d ]/', $postcode))
        {
            $this->errorMessage .= '<p class = "errorMessage">Postcode contains invalid characters.</p>';
        }
    }

    private function validateCity($city)
    {
        if ($this->isEmpty($city))
        {
            $this->errorMessage .= '<p class = "errorMessage">You didn\'t enter city.</p>';
        }
        else if ($this->isTooLong($city, 50))
        {
            $this->errorMessage .= '<p class = "errorMessage">City is too long.</p>';
        }
        else if (preg_match('/[^a-zA-Z ]/', $city))
        {
            $this->errorMessage .= '<p class = "errorMessage">City contains invalid characters.</p>';
        }
    }

    private function validateStreet($street)
    {
        if ($this->isEmpty($street))
        {
            $this->errorMessage .= '<p class = "errorMessage">You didn\'t enter street.</p>';
        }
        else if ($this->isTooLong($street, 50))
        {
            $this->errorMessage .= '<p class = "errorMessage">Street name is too long.</p>';
        }
        else if (preg_match('/[^A-Za-z0-9 ]/', $street))
        {
            $this->errorMessage .= '<p class = "errorMessage">Street contains invalid characters.</p>';
        }
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    private function isEmpty($string)
    {
        return strlen($string) <= 0;
    }

    private function isTooLong($string, $maxLength)
    {
        return strlen($string) > $maxLength;
    }

    private function isTooShort($string, $minLength)
    {
        return strlen($string) < $minLength;
    }

}
