<?php
require_once 'header.php';
require_once 'customer.php';

if (!isset($_SESSION['user']))
{
    header("Location: index.php");
}

if (isset($_GET['action']) && isset($_GET['id']) && isset($_GET['quantity']))
{
    if ($_GET['action'] === 'add')
    {
        $userId = $_SESSION['user']->id;
        $id = $_GET['id'];
        $quantity = $_GET['quantity'];

        if (!preg_match('/\D/', $id) && !preg_match('/\D/', $quantity))
        {
            // Check stock available.
            $query = "SELECT * FROM stock WHERE wine_id = '" . $id . "';";
            $result = $db->query($query);
            if ($result->num_rows === 1) // If wine exists in stock...
            {
                $rowStockItem = $result->fetch_assoc();
            }

            if ($rowStockItem['quantity'] >= $quantity) // If more stock than needed...
            {
                // Check if already in basket.
                $query = "SELECT * FROM shopping_basket "
                        . "WHERE customer_id = '" . $userId . "' "
                        . "AND wine_id = '" . $id . "';";
                $result = $db->query($query);

                if ($result->num_rows === 1) // If yes...
                {
                    echo "Updating basket item...";
                    $rowShoppingBasketItem = $result->fetch_assoc();
                    $currentQuantity = $rowShoppingBasketItem['quantity'];
                    $newQuantity = $currentQuantity + $quantity;

                    $query = "UPDATE shopping_basket SET quantity = '" . $newQuantity . "' WHERE customer_id = '" . $userId . "' AND wine_id = '" . $id . "';";
                    $db->query($query);
                    echo $db->error;
                }
                else // If no...
                {
                    // Add to customer's basket.
                    $query = "INSERT INTO shopping_basket VALUES (?, ?, ?);";
                    if (!$stmt = $db->prepare($query))
                    {
                        echo $db->error;
                        echo "Couldn't prepare statement";
                    }
                    else if (!$stmt->bind_param('iii', $userId, $id, $quantity))
                    {
                        echo $stmt->error;
                    }
                    else if (!$stmt->execute())
                    {
                        echo $stmt->error;
                    }
                    $stmt->close();
                    //header("Location: " . $_SERVER['HTTP_REFERER']);
                }

                // Update distribution centre's stock.
                $newQuantity = $rowStockItem['quantity'] - $quantity;

                $query = "UPDATE stock SET quantity = '" . $newQuantity . "' WHERE wine_id = '" . $id . "';";
                $db->query($query);
            }
        }
    }
}
else if (isset($_GET['action']) && $_GET['action'] === 'remove')
{
    $userId = $_SESSION['user']->id;
    $id = $_GET['id'];

    $query = "DELETE FROM shopping_basket WHERE wine_id = '" . $id . "' AND customer_id = '" . $userId . "';";
    $db->query($query);
    echo $db->error;
}

$query = "SELECT * FROM shopping_basket WHERE customer_id = '" . $_SESSION['user']->id . "';";

$result = $db->query($query);
if ($result->num_rows > 0)
{
    while ($row = $result->fetch_assoc())
    {
        $query = "SELECT * FROM wine WHERE id = '" . $row['wine_id'] . "';";
        $wines = $db->query($query);
        if ($wines->num_rows > 0)
        {
            $wine = $wines->fetch_assoc();
            displayWine($wine, $row['quantity']);
        }
    }
}

function displayWine($wine, $qty)
{
    ?>
    <div class = "wine">
        <div class = "wine_description">
            <h1><?php echo $wine['name']; ?></h1>
            <p><?php echo $wine['description']; ?></p>
        </div>
        <div class = "wine_details">
            <p>Price: <?php echo $wine['bottle_price']; ?></p>
            <p>Country of origin: <?php echo $wine['country']; ?></p>
            <p>In basket: <?php echo $qty; ?> </p>

            <?php
            $link = "basket.php?action=remove&amp;id=" . $wine['id'];
            echo HTML::anchor($link, "Remove");
            ?>
        </div>
    </div>
    <?php
}
