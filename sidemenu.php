<nav id = "side_menu">
    <ul>
        <li><a href = "search.php?type=wine">Wines</a>
            <ul>
                <li><a href = "search.php?colour=red">Red</a>
                <li><a href = "search.php?colour=white">White</a>
            </ul>
            
            <ul>
                <li><a href = "search.php?type=wine&amp;country=france">French</a></li>
                <li><a href = "search.php?type=wine&amp;country=germany">German</a></li>
                <li><a href = "search.php?type=wine&amp;country=italy">Italian</a></li>
                <li><a href = "search.php?type=wine&amp;country=portugal">Portugal</a></li>
                <li><a href = "search.php?type=wine&amp;country=new+zealand">New Zealand</a></li>
                <li><a href = "search.php?type=wine&amp;country=spain">Spanish</a></li>
                <li><a href = "search.php?type=wine&amp;country=south+africa">South African</a></li>
                <li><a href = "search.php?type=wine&amp;country=argentina">Argentinian</a></li>
                <li><a href = "search.php?type=wine&amp;country=chile">Chilean</a></li>
            </ul>
        </li>
        <li><a href = "search.php?type=champagne">Champagnes</a>
            <ul>
                <li><a href = "search.php?type=champagne&amp;country=france">French</a></li>
                <li><a href = "search.php?type=champagne&amp;country=italy">Italian</a></li>
                <li><a href = "search.php?type=champagne&amp;country=champagne">Champagne</a></li>
            </ul>
        </li>
    </ul>
</nav>