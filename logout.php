<?php
	session_start();
	
	if (isset($_SESSION['user']))
	{
		unset($_SESSION['user']);
	}

	$_SESSION['message'] = "Logged out";
	header('Location: ' . $_SERVER['HTTP_REFERER']);
?>