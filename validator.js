function validate()
{
    // Clear previous error messages
    var errorMessages = document.getElementsByClassName('errorMessage');
    for (var i = 0; i < errorMessages.length; i++)
    {
        errorMessages[i].parentNode.removeChild(errorMessages[i]);
    }

    return validateUsername() &&
            validateEmail() &&
            validatePassword() &&
            validateName() &&
            validateAddress();
}

function validateEmail()
{
    var email = document.getElementById('email');
    var emailConfirm = document.getElementById('email_confirm');
    if (isEmpty(email))
    {
        insertErrorMessage('Email is required.', email);

        return false;
    }
    else if (!/[^\s@]+@[^\s@]+\.[^\s@]+/.test(email.value))
    {
        insertErrorMessage('Valid email is required.', email);

        return false;
    }
    else if (emailConfirm.value !== email.value)
    {
        insertErrorMessage('Confirm your email.', emailConfirm);

        return false;
    }
    return true;
}

function validateUsername()
{
    var username = document.getElementById('username');

    if (isEmpty(username))
    {
        insertErrorMessage('Username is required.', username);

        return false;
    }
    else if (username.value.length > 16)
    {
        insertErrorMessage('Username \'-\' must not be longer than 16 characters.', username);

        return false;
    }
    else if (username.value.length < 3)
    {
        insertErrorMessage('Username \'-\' must not be shorter than 3 characters.', username);

        return false;
    }
    else if (/[^a-zA-Z0-9_]+/.test(username.value))
    {
        insertErrorMessage('Username is invalid. Only numbers, letters and \'_\' are valid characters.', username);

        return false;
    }
}

function validatePassword()
{
    var password = document.getElementById('password');
    var passwordConfirm = document.getElementById('password_confirm');

    if (isEmpty(password))
    {
        insertErrorMessage('Password is required.', password);

        return false;
    }
    else if (password.value.length > 32)
    {
        insertErrorMessage('Password must not be longer than 32 characters.', password);

        return false;
    }
    else if (password.value.length < 6)
    {
        insertErrorMessage('Password must be at least 6 characters long.', password);

        return false;
    }
    else if (/[^a-zA-Z0-9_]+/.test(password.value))
    {
        insertErrorMessage('Your password contains invalid characters. Only letters, digits and \'_\' are allowed.', password);

        return false;
    }
    else if (password.value !== passwordConfirm.value)
    {
        insertErrorMessage('Confirm your password.', passwordConfirm);

        return false;
    }

    return true;
}

function validateName()
{
    if (!validateFirstName() || !validateLastName())
    {
        return false;
    }


    return true;
}

function validateFirstName()
{
    var fName = document.getElementById('first_name');

    if (isEmpty(fName))
    {
        insertErrorMessage('First name is required.', fName);

        return false;
    }
    else if (fName.value.length < 2)
    {
        insertErrorMessage('First name must be longer than 2 characters.', fName);

        return false;
    }
    else if (/[^A-Za-z]/.test(fName.value))
    {
        insertErrorMessage('First name contains invalid characters. Only leters are allowed.', fName);

        return false;
    }

    return true;
}

function validateLastName()
{
    var lName = document.getElementById('last_name');

    if (isEmpty(lName))
    {
        insertErrorMessage('Last name is required.', lName);

        return false;
    }
    else if (lName.value.length < 2)
    {
        insertErrorMessage('Last name must be longer than 2 characters.', lName);

        return false;
    }
    else if (/[^A-Za-z]/.test(lName.value))
    {
        insertErrorMessage('First name contains invalid characters. Only leters are allowed.', lName);

        return false;
    }

    return true;
}

function validatePostcode()
{
    var postcode = document.getElementById('postcode');

    if (isEmpty(postcode))
    {
        insertErrorMessage('Postcode is required.', postcode);

        return false;
    }
    else if (postcode.value.length > 9)
    {
        insertErrorMessage('Postcode is too long.', postcode);

        return false;
    }
    else if (/[^a-zA-Z\d ]/.test(postcode.value))
    {
        insertErrorMessage('Postcode contains invalid characters.', postcode);

        return false;
    }

    return true;
}

function validateAddress()
{
    if (!validateCity() || !validateStreet() || !validatePostcode())
    {
        return false;
    }

    return true;
}

function validateCity()
{
    var city = document.getElementById('city');

    if (isEmpty(city))
    {
        insertErrorMessage('City is required.', city);

        return false;
    }
    else if (isTooLong(city, 50))
    {
        insertErrorMessage('Field contains too many characters.', city);

        return false;
    }
    else if (/[^a-zA-Z ]/.test(city.value))
    {
        insertErrorMessage('Field contains invalid characters.', city);

        return false;
    }

    return true;
}

function validateStreet()
{
    var street = document.getElementById('street');

    if (isEmpty(street))
    {
        insertErrorMessage('Street is required.', street);

        return false;
    }
    else if (isTooLong(street, 50))
    {
        insertErrorMessage('You entered too many characters.', street);

        return false;
    }
    if (/[^A-Za-z0-9 ]/.test(street.value))
    {
        insertErrorMessage('Invalid characters detected.', street);

        return false;
    }

    return true;
}

function isEmpty(element)
{
    return element.value.length <= 0;
}

function isTooLong(element, maxLength)
{
    return element.value.length > maxLength;
}

// Insert new element containing error message after specified element.
function insertErrorMessage(message, element)
{
    var span = document.createElement('span');
    span.setAttribute('class', 'errorMessage');
    span.innerHTML = message;
    element.parentNode.insertBefore(span, element);
}