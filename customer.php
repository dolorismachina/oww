<?php
require_once 'address.php';
class Customer 
{
    public $username;
    public $firstName;
    public $lastName;
    public $password;
    public $address;
    public $email;
    public $id;
    
    /**
     * 
     * @param type $dbArray Array resulting from call to mysqli_result::fetch_assoc.
     */
    public function __construct($dbArray)
    {
        $this->username = $dbArray['username'];
        $this->firstName = $dbArray['f_name'];
        $this->lastName = $dbArray['l_name'];
        $this->address = new Address($dbArray);
        $this->email = $dbArray['email_address'];
        $this->id = $dbArray['id'];
    }
}
