<?php

/**
 * Helper class to allow insertion of common HTML tags from php code without 
 * having to mix it with HTML.
 */
class HTML
{
    public static function anchor($target, $text)
    {
        return '<a href = "' . $target . '">' . $text . '</a>';
    }
    public static function a($target, $text)
    {
        return '<a href = "' . $target . '">' . $text . '</a>';
    }
    
    public static function paragraph($content)
    {
        return '<p>' . $content . '</p>';
    }
    public static function p($content)
    {
        return '<p>' . $content . '</p>';
    }
}