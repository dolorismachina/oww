<?php

class Address
{
    public $street;
    public $postcode;
    public $city;
    
    public function __construct($dbArray)
    {
        $this->street = $dbArray['street'];
        $this->city = $dbArray['town'];
        $this->postcode = $dbArray['postcode'];
    }
}