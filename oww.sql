-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2014 at 08:57 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oww`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `town` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `f_name`, `l_name`, `username`, `password`, `street`, `town`, `postcode`, `email_address`, `telephone`) VALUES
(1, 'Radoslaw', 'Polczyk', 'kingston_student', 'student_password', '133 Albert Road', 'London', 'E10 6PA', 'k1022983@gmail.com', '07932996771');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

CREATE TABLE IF NOT EXISTS `customer_order` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `customer_id` int(8) DEFAULT NULL,
  `order_date` date NOT NULL,
  `delivery_charge` double(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `distribution_centre`
--

CREATE TABLE IF NOT EXISTS `distribution_centre` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `town` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE IF NOT EXISTS `order_item` (
  `wine_id` int(8) NOT NULL DEFAULT '0',
  `customer_order_id` int(8) NOT NULL DEFAULT '0',
  `quantity` int(5) DEFAULT NULL,
  PRIMARY KEY (`wine_id`,`customer_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE IF NOT EXISTS `promotion` (
  `promotion_id` int(2) NOT NULL,
  `promotion_description` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`promotion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotion_wine`
--

CREATE TABLE IF NOT EXISTS `promotion_wine` (
  `promotion_id` int(2) NOT NULL DEFAULT '0',
  `wine_id` int(8) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`promotion_id`,`wine_id`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shopping_basket`
--

CREATE TABLE IF NOT EXISTS `shopping_basket` (
  `wine_id` int(8) DEFAULT NULL,
  `customer_id` int(8) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `distribution_centre_id` int(3) NOT NULL,
  `wine_id` int(8) NOT NULL,
  `quantity` int(4) NOT NULL,
  PRIMARY KEY (`distribution_centre_id`,`wine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wine`
--

CREATE TABLE IF NOT EXISTS `wine` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_available` date NOT NULL,
  `bottle_price` double(7,2) NOT NULL,
  `colour` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `case_price` double(7,2) NOT NULL,
  `case_size` int(2) NOT NULL,
  `sweetness` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wine`
--

INSERT INTO `wine` (`id`, `name`, `description`, `type`, `country`, `date_available`, `bottle_price`, `colour`, `case_price`, `case_size`, `sweetness`) VALUES
(1, 'Fritz''s Riesling, Gunderloch 2012 Rheinhessen', 'Fritz''s Riesling comes from vineyards from a small area in the Rheinhessen called the ''Roter Hang,'' or red hill, just outside the village of Nackenheim. The red slate soil is mineral rich, combining with a southerly aspect to provide perfect conditions for', 'wine', 'germany', '2014-02-03', 9.99, 'white', 54.99, 6, NULL),
(2, 'L''instant Truffier Malbec 2012 Rigal, PGI Côtes du', 'One of the premier producers of Cahors, the Rigal family have extended their operation to include vineyards throughout the Lot Valley. The region has many alluvial terraces that are free-draining and have great sun exposure, and are perfect for growing Mal', 'wine', 'france', '2014-02-28', 9.99, 'red', 54.99, 6, NULL),
(3, 'Quinta de Azevedo 2012 Vinho Verde', 'Literally ''green wine'' from the Minho region of Northern Portugal. Historically fermented in open stone lagares before being run off into cask and undergoing malolactic fermentation. Fermentation now occurs in vat, retaining the classic spritz or CO2 bubbl', 'wine', 'portugal', '2014-02-02', 8.99, 'white', 49.99, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `wine_id` int(8) NOT NULL DEFAULT '0',
  `customer_id` int(8) NOT NULL DEFAULT '0',
  `date_added` date NOT NULL,
  PRIMARY KEY (`wine_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
